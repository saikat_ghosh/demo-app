## About the project

A simple 'single user, multiple app' project made using Laravel framework which is open to multiple apps to access. The objective is to authenticate a user for multiple apps and then return user details based on authentication token.

## Getting Started

 1. Clone the project from remote repository,
 2. Go to the application folder using `cd`,
 3. Run `composer install` on your cmd or terminal,
 4. Create `.env` file from `.env.example` on root folder. You can type `copy .env.example .env` if using command prompt Windows or `cp .env.example .env` if using terminal Ubuntu
 5. Open your `.env` file and change the database name (DB_DATABASE) to your actual database name, username (DB_USERNAME) and password (DB_PASSWORD) field correspond to your configuration. 
    By default, username is `root` and you can leave password field empty,
 6. Run `php artisan key:generate`,
 7. Run `php artisan migrate`,
 8. Run `php artisan serve`
 
## Running the project

 1. `Registering a user` :  make a 'POST' request to 'api / register' with fields 'user_id'' and 'password' (both required); user_id needs to be unique for each user.
 2. `Logging in as user` : make a 'POST' request to 'api / login' with the same fields 'user_id'' and 'password' (both required) the user has registered with; the user should receive a 60 character token which will be used for authentication.
 3. `Retrieving user-details` : make a 'POST' request to 'api / user-details' with the field 'token' which contains the 60 char long token received after successful login. The user should receive the user details after successful authentication.
 4. `Getting names of all apps sending API request` : you can do this in two ways-
        `(a)` send a 'POST' request to 'dashboard' with the field 'user_id' containing the valid user_id of the user you want to track,
        `(b)` Go to 'localhost:8000', enter 'user_id' of the user you want to track.

## Code Highlights

 1. `Token Expiry Time` : a private variable 'token_expiry_time' has been defined inside 'ApiController'; default value is set to 5 minutes; feel free to set it's value as per requirement.
 2. `Routes` : routes for 'register', 'login' and 'user-details' have been defined in 'api.php' while the route for 'dashboard' is defined in 'web.php'.