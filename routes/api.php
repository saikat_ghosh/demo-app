<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* registering new user */
Route::post('register', 'ApiController@register');

/* logging user in */
Route::post('login', 'ApiController@login');

/* retrieving user details by token */
Route::post('user-details', 'ApiController@authenticate');



