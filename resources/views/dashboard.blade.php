<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->

    <!-- BOOTSTRAP STYLES-->
    <link href={{asset("/css/app.css")}} rel="stylesheet" />
    <!-- FONT-AWESOME STYLES-->
    <link href={{asset("/css/font-awesome.css")}} rel="stylesheet" />
    <!-- CUSTOM STYLES-->
    <link href="{{asset("/css/customStyles.css")}}" rel="stylesheet">
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <!-- GLYPHICON STYLES -->
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
    <!-- Page Specific Additional Styles goes here -->
    <style>@stack('css')</style>
    <!-- Scripts ->
        <script src="{{asset('/js/app.js')}}"></script-->
    <script src="{{url('http://code.jquery.com/jquery-1.11.0.min.js')}}"></script>
    <script src="{{url('//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js')}}"></script>
    <!-- Page Specific Additional Javascript codes goes here -->
    @stack('js')
</head>

<body>
<div id="app" class="content">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'demo-app') }}
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    &nbsp;
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                <span class="glyphicon glyphicon-user"></span>{{ $user->user_id }} <span class="caret"></span>
                            </a>
                        </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- End of Navigation Bar -->

    <!-- Main Content goes here -->
    <div class="container">
        <div class="row padding-bottom">
            <center>
                <img src="{{asset('logo.png')}}">
            </center>
        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <center><strong>Total number of tokens consumed : <span class="badge">{{ $totalTokens }}</span> </strong></center>
                    </div>
                    <div class="panel-body">
                        <ul class="list-group">
                            @foreach($tokens as $token)
                                <li class="list-group-item"><strong>{{ $token->app_name }}</strong>&nbsp;:&nbsp;<span class="badge">{{ $token->total }}</span> </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</html>
