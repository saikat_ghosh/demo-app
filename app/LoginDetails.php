<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoginDetails extends Model
{
    protected $table = 'login_details';
    protected $fillable = [
        'user_id', 'app_name', 'api_token',
    ];
}
