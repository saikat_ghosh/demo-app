<?php

namespace App\Http\Controllers;

use App\LoginDetails;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ApiController extends Controller
{
    //sets the token expiry time
    private $token_expiry_time = '5 minutes';

    /*
    |------------------------------------------------------------------------------
    | Method for retrieving user instance by user_id
    |------------------------------------------------------------------------------
    */
    public function getUserByUserId($user_id)
    {
        $user = DB::table('users')->where([['user_id',$user_id]])->first();
        return $user;
    }

    /*
    |------------------------------------------------------------------------------
    | Method for retrieving the domain name from which the API call is made
    |------------------------------------------------------------------------------
    */
    public function getAppName(Request $request)
    {
        $app_name = gethostbyaddr($request->getClientIp());
        return $app_name;
    }

    /*
    |------------------------------------------------------------------------------
    | Methods for registering new user
    |------------------------------------------------------------------------------
    */
    public function register(Request $request)
    {
        if($request->has('user_id') && $request->has('password'))
        {
            $user = $this->getUserByUserId($request->user_id);

            if($user)
                return response()->json(['error'=>true, 'data'=>['message'=>'user with same ID already exists.']],400);
            else
            {
                $user = new User();
                $user->user_id = $request->user_id;
                $user->password = bcrypt($request->password);

                if($user->save())
                    return response()->json(['error'=>false, 'data'=>['message'=>'user registered successfully.']], 201);
                else
                    return response(['error'=>true, 'data'=>['message'=>'could not register user.']],500);
            }
        }
        else
            return response()->json(['error'=>true, 'data'=>['message'=>'both user_id & password are required.']],400);
    }

    /*
    |------------------------------------------------------------------------------
    | Method for user login
    |------------------------------------------------------------------------------
    */
    public function login(Request $request)
    {
        $app_name = $this->getAppName($request);

        if($request->has('user_id') && $request->has('password'))
        {
            $user = $this->getUserByUserId($request->user_id);

            if($user && Hash::check($request->password, $user->password))
            {
                $login_details = $this->getToken($app_name,$user);

                if($login_details)
                {
                    if($this->tokenExpired($login_details))
                        $token = $this->generateToken($user->id,$app_name);
                    else
                        $token = $login_details->api_token;
                }
                else
                    $token = $this->generateToken($user->id,$app_name);

                if($token)
                    return response()->json(['error'=>false, 'data'=>['message'=>'logged in successfully.','token'=>$token]],200);
                else
                    return response()->json(['error'=>true, 'data'=>['message'=>'could not create token.']],500);
            }
            else
                return response()->json(['error'=>true, 'data'=>['message'=>'wrong login credentials.']],401);
        }
        else
            return response()->json(['error'=>true, 'data'=>['message'=>'both user_id & password are required.']],400);
    }

    /*
    |------------------------------------------------------------------------------
    | Method for retrieving latest token for the current user for the current app
    |------------------------------------------------------------------------------
    */
    public function getToken($app_name,$user)
    {
        $login_details = DB::table('login_details')->where([['app_name',$app_name],['user_id',$user->id]])->orderBy('id','DESC')->first();

        return $login_details;
    }

    /*
    |------------------------------------------------------------------------------
    | Methods for generating new token for the user for the current app
    |------------------------------------------------------------------------------
    */
    public function generateToken($user_id,$app_name)
    {
        $login_details = new LoginDetails;
        $login_details->user_id = $user_id;
        $login_details->app_name = $app_name;
        $login_details->api_token = str_random(60);

        if($login_details->save())
        {
            return $login_details->api_token;
        }
        else
            return null;
    }

    /*
    |------------------------------------------------------------------------------
    | Method for authenticating the user and return user details
    |------------------------------------------------------------------------------
    */
    public function authenticate(Request $request)
    {
        if($request->has('token'))
        {
            $token = $request->token;

            $app_name = $this->getAppName($request);

            $login_details = $this->verifyToken($app_name, $token);

            if($login_details)
            {
                if($this->tokenExpired($login_details))
                    return response()->json(['error'=>true, 'data'=>['message'=>'token expired. login again.']],400);
                else
                {
                    $user = DB::table('users')->where('id',$login_details->user_id)->first();
                    return response()->json(['error'=>false, 'data'=>['message'=>'successfully authenticated.','user'=>$user]],200);
                }
            }
            else
                return response()->json(['error'=>true, 'data'=>['message'=>'unauthenticated user.']],401);

        }
        else
            return response()->json(['error'=>true, 'data'=>['message'=>'token missing.']],400);
    }

    /*
    |------------------------------------------------------------------------------
    | Method for verifying current user token
    |------------------------------------------------------------------------------
    */
    public function verifyToken($app_name,$token)
    {
        $login_details = DB::table('login_details')->where([['app_name',$app_name],['api_token',$token]])->first();

        return $login_details;
    }

    /*
    |------------------------------------------------------------------------------
    | Method for verifying if the user token is already expired
    |------------------------------------------------------------------------------
    */
    public function tokenExpired($login_details)
    {
        $timestamp = strtotime('-' . $this->token_expiry_time);

        $valid_token_generation_time = date('Y-m-d H:i:s',$timestamp);

        if($login_details->created_at < $valid_token_generation_time)
            return true;
        else
            return false;
    }

    /*
    |-------------------------------------------------------------------------------------
    | Method for returning the name of the apps and the number of tokens used by each app
    |-------------------------------------------------------------------------------------
    */
    public function dashboard(Request $request)
    {
        if($request->has('user_id'))
        {
            $user = $this->getUserByUserId($request->user_id);

            if($user)
            {
                $totalTokens = DB::table('login_details')->where('user_id',$user->id)->count();

                $tokens = DB::table('login_details')->where('user_id',$user->id)->select('app_name','user_id',DB::raw('count(*) as total'))->groupBy('app_name')->get();

                return view('dashboard')->with(['user'=>$user,'totalTokens'=>$totalTokens, 'tokens'=>$tokens]);
            }
            else
                return response()->json(['error'=>'true','data'=>['message'=>'invalid user id.']],400);
        }
        else
            return response()->json(['error'=>'true','data'=>['message'=>'user_id missing.']],400);
    }
}
